package HelicopterGame;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.AudioClip;

/**
 * Created by Grant Terrien
 *
 * View/Sprite for the Helicopter
 */
public class HelicopterView extends Group {
    private ImageView imageView;
    private AudioClip helicopterSound;
    private AudioClip helicopterCrash;

    public HelicopterView() {
        Image image = new Image(getClass().getResourceAsStream("res/helicopter.gif"));
        imageView = new ImageView();
        imageView.setImage(image);
        imageView.setFitWidth(50.0);
        imageView.setFitHeight(50.0);


        this.getChildren().add(imageView);

        helicopterSound = new AudioClip(getClass().getResource("res/helicoptersound.wav").toString());
        helicopterCrash = new AudioClip(getClass().getResource("res/helicoptercrash.wav").toString());



    }

    public AudioClip getHelicopterSound() {
        return helicopterSound;
    }

    public AudioClip getHelicopterCrash() {
        return helicopterCrash;
    }

    public Point2D getPosition() {
        Point2D position = new Point2D(this.getLayoutX(), this.getLayoutY());
        return position;
    }

    public void setPosition(double x, double y) {
        this.setLayoutX(x);
        this.setLayoutY(y);
    }

    /**
     * Change image displayed to a "dead" helicopter.
     */
    public void helicopterDies() {
        this.imageView.setImage(new Image(getClass().getResourceAsStream("res/deadhelicopter.gif")));

    }


}
