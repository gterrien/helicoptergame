package HelicopterGame;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Grant Terrien
 *
 * Holds scores attained by the user previously.
 * Serializable so that this data can be saved to disk.
 */
public class ScoreList implements java.io.Serializable {
    private ArrayList<Integer> scores;

    ScoreList() {
        scores = new ArrayList<Integer>();
    }

    public ArrayList<Integer> getScores() {
        return this.scores;
    }


    /**
     * Serializes the ScoreList object to scoreList.ser
     */
    public void save() {
        try
        {
            FileOutputStream fileOut =
                    new FileOutputStream("scoreList.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this);
            out.close();
            fileOut.close();
           // System.out.println("Serialized data is saved in ScoreList.ser");
        }catch(IOException i)
        {
            i.printStackTrace();
        }
    }

    /**
     * Deserializes the ScoreList from scoreList.ser
     *
     * @return the loaded ScoreList
     */
    public static ScoreList load() {
        try
        {
            FileInputStream fileIn = new FileInputStream("scoreList.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            ScoreList scoreList = (ScoreList) in.readObject();
            in.close();
            fileIn.close();
            return scoreList;
        }catch(IOException i)
        {
            System.out.println("No ScoreList file found");
            return null;
        }catch(ClassNotFoundException c)
        {
            System.out.println("ScoreList class not found");
            c.printStackTrace();
            return null;
        }
    }

    /**
     * Adds a new score to the score list.
     * @param score to be added to the score list
     */
    public void addNewScore(int score) {
        score = new Integer(score);
        scores.add(score);
    }

    /**
     * Converts the ScoreList to an array.
     * @return a simple int array that contains all of the scores achieved in the order they occurred
     */
    public int[] toIntArray() {
        int[] array = new int[this.getScores().size()];
        for (int i = 0; i < this.getScores().size(); i++) {
            array[i] = this.getScores().get(i);
        }

        return array;
    }

}
