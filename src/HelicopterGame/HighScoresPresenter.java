package HelicopterGame;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import javafx.scene.input.MouseEvent;
import java.util.Arrays;

/**
 * Created by Grant Terrien
 *
 * Controller for the High Scores view.
 */
public class HighScoresPresenter {
    private ScoreList scoreList;
    private int[] unsortedArray;
    private int[] array;
    private Group root;

    final int xAlign = 300;


    public void setRoot(Group root) {
        this.root = root;
    }

    public HighScoresPresenter() {
        // Load the list of previous scores, if possible
        scoreList = ScoreList.load();
        if (scoreList == null) {
            // Else make a new score list, fill it with 0s, and save it
            scoreList = new ScoreList();
            scoreList.addNewScore(0);
            scoreList.addNewScore(0);
            scoreList.addNewScore(0);
            scoreList.save();
        } else if (scoreList.getScores().size() < 3) {
            scoreList.addNewScore(0);
            scoreList.addNewScore(0);
            scoreList.addNewScore(0);
            scoreList.save();
        }

        array = scoreList.toIntArray();

        unsortedArray = array.clone();


        Arrays.sort(array);

    }


    /**
     * Loads and displays the view for the main menu screen
     */
    public void loadStartScreen(Event event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("StartScreen.fxml"));/* Exception */
        } catch (Exception e) {
            System.out.println("Error loading StartScreen.fxml");
        }
        Scene scene = new Scene(root, 640, 360);
        stage.setScene(scene);
        stage.show();
    }


    /**
     * Draws a title for the screen and the labels for the top three high scores
     * AND a button which goes back to the home screen
     */
    public void createScoreLabels() {
        addPageTitle();
        addScoreLabel1();
        addScoreLabel2();
        addScoreLabel3();
        addBackToHomeButton();
    }

    /**
     * Adds title text to the high scores page
     */
    public void addPageTitle() {
        Label pageTitle = new Label();
        pageTitle.setText("High Scores");
        pageTitle.setScaleX(3.0);
        pageTitle.setScaleY(3.0);
        pageTitle.setLayoutX(xAlign);
        pageTitle.setLayoutY(70.0);
        root.getChildren().add(pageTitle);

    }


    /**
     * Draws the top high score on the screen and how many games have been played since that score was achieved
     */
    public void addScoreLabel1() {
        final int yAlign = 150;
        Label score1 = new Label();
        score1.setText("1. " + Integer.toString(array[array.length-1]));
        score1.setLayoutX(xAlign);
        score1.setLayoutY(yAlign);
        score1.setScaleX(2.0);
        score1.setScaleY(2.0);

        Label gamesSinceLabel1 = new Label();
        int gamesSince1 = numGamesSince(1);
        gamesSinceLabel1.setText(Integer.toString(gamesSince1) + " game(s) played since this score");
        gamesSinceLabel1.setLayoutX(xAlign + 90);
        gamesSinceLabel1.setLayoutY(yAlign);

        root.getChildren().add(score1);
        root.getChildren().add(gamesSinceLabel1);
    }


    /**
     * Draws the second-to-top high score on the screen and how many games have been played since that score was achieved
     */
    public void addScoreLabel2() {
        final int yAlign = 180;
        Label score2 = new Label();
        score2.setText("2. " + Integer.toString(array[array.length-2]));
        score2.setLayoutX(xAlign);
        score2.setLayoutY(yAlign);
        score2.setScaleX(2.0);
        score2.setScaleY(2.0);

        Label gamesSinceLabel2 = new Label();
        int gamesSince2 = numGamesSince(2);

        gamesSinceLabel2.setText(Integer.toString(gamesSince2) + " game(s) played since this score");
        gamesSinceLabel2.setLayoutX(xAlign + 90);
        gamesSinceLabel2.setLayoutY(yAlign);

        root.getChildren().add(score2);
        root.getChildren().add(gamesSinceLabel2);
    }


    /**
     * Draws the third-to-top high score on the screen and how many games have been played since that score was achieved
     */
    public void addScoreLabel3() {
        final int yAlign = 210;
        Label score3 = new Label();
        score3.setText("3. " + Integer.toString(array[array.length-3]));
        score3.setLayoutX(xAlign);
        score3.setLayoutY(yAlign);
        score3.setScaleX(2.0);
        score3.setScaleY(2.0);

        Label gamesSinceLabel3 = new Label();
        int gamesSince3 = numGamesSince(3);
        gamesSinceLabel3.setText(Integer.toString(gamesSince3) + " game(s) played since this score");
        gamesSinceLabel3.setLayoutX(xAlign + 90);
        gamesSinceLabel3.setLayoutY(yAlign);

        root.getChildren().add(score3);
        root.getChildren().add(gamesSinceLabel3);
    }

    /**
     * Adds a button that links to the start menu
     */
    public void addBackToHomeButton() {
        Button backToHome = new Button();
        backToHome.setText("Back");
        backToHome.setLayoutX(300.0);
        backToHome.setLayoutY(280.0);
        backToHome.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                loadStartScreen(mouseEvent);
            }
        });
        backToHome.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                loadStartScreen(keyEvent);
            }
        });

        root.getChildren().add(backToHome);

    }

    /**
     *
     * @param rank the number rank of a score (1 is the highest)
     * @return the number of games played since that score was achieved
     */
    public int numGamesSince(int rank) {
        int gamesSince = 0;
        for (int i = 0; i < unsortedArray.length; i++) {
            if (unsortedArray[i] == array[array.length-rank]) {
                gamesSince = unsortedArray.length-1-i;
            }
        }
        return gamesSince;
    }

}
