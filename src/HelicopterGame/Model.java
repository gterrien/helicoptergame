package HelicopterGame;

import javafx.geometry.Point2D;
import java.util.ArrayList;
import java.util.Random;


/**
 * Created by Grant Terrien
 *
 * Contains and regulates the overall game state/world for the game.
 */
public class Model {
    private Helicopter helicopter;
    private ArrayList<Wall> wallList;
    private int currentScore;
    private ScoreList scoreList;

    final private int GRAVITY = 1;

    public Model() {
        currentScore = 0;
        helicopter = new Helicopter(new Point2D(100, 230));
        helicopter.setYVelocity(-40);
        wallList = new ArrayList<Wall>();


        // Load the list of previous scores, if possible
        scoreList = ScoreList.load();
        if (scoreList == null) {
            // Else make a new score list
            scoreList = new ScoreList();
        }


    }


    // Getters
    public Helicopter getHelicopter() {
        return helicopter;
    }

    public ArrayList<Wall> getWallList() {
        return this.wallList;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public ScoreList getScoreList() {
        return this.scoreList;
    }




    /**
     * Updates the positions of the objects in the game world (the helicopter and the walls)
     * i.e. moves all the walls to the left, etc.
     */
    public void step() {
        currentScore += 1;
        actGravityOnHelicopter();
        double addToHeight = helicopter.getYVelocity()/6.5;
        helicopter.setLocationPoint(new Point2D(helicopter.getLocationPoint().getX(),
                helicopter.getLocationPoint().getY() + addToHeight));
        for (Wall wall : wallList) {
            wall.setLocationPoint(new Point2D(wall.getLocationPoint().getX() - 5, wall.getLocationPoint().getY()));
        }
    }


    /**
     * Creates a new Wall object. Wall's height and vertical position are
     * random, but within constraints so as to never make the game unbeatable.
     */
    public Wall createNewWall() {
        Random rand = new Random();
        int height = rand.nextInt(100) + 100;
        int y = rand.nextInt(360-height);
        Wall wall = new Wall(new Point2D(640,y),height);
        wallList.add(wall);
        return wall;
    }

    /**
     *
     * @return if the helicopter has collided with either a wall or the ceiling/floor
     */
    public boolean helicopterHasCollided() {
        for (Wall wall : wallList) {
            if (wall.getBoundingBox().intersects(helicopter.getBoundingBox())) {
                return true;
            } else if (helicopter.getLocationPoint().getY() <= 0) {
                return true;
            } else if (helicopter.getLocationPoint().getY() >= 310) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds to the helicopter's y-velocity
     */
    public void raiseHelicopter() {
        if (this.helicopter.getYVelocity() >= -30) {
            this.helicopter.setYVelocity(this.helicopter.getYVelocity() - 2);
        }
    }

    /**
     * Subtracts from the helicopter's y-velocity
     */
    public void actGravityOnHelicopter() {
        if (this.helicopter.getYVelocity() <= 30) {
            this.helicopter.setYVelocity(this.helicopter.getYVelocity() + GRAVITY);
        }
    }





}
