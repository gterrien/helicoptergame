package HelicopterGame;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;

/**
 * Created by Grant Terrien
 *
 * Represents a wall, which serve as obstacles for the player character.
 */
public class Wall {
    private Point2D locationPoint;
    private int height;
    private BoundingBox boundingBox;

    public Wall(Point2D locationPoint, int height) {
        this.locationPoint = locationPoint;
        this.height = height;
        this.boundingBox = new BoundingBox(locationPoint.getX(),locationPoint.getY(),50,height);
    }

    public Point2D getLocationPoint() {
        return locationPoint;
    }

    public void setLocationPoint(Point2D locationPoint) {
        this.locationPoint = locationPoint;
        this.boundingBox = new BoundingBox(locationPoint.getX(),locationPoint.getY(),50,height);
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public BoundingBox getBoundingBox() {
        return this.boundingBox;
    }
}
