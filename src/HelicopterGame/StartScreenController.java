package HelicopterGame;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * Created by Grant Terrien
 *
 * Controller for the start screen view
 */
public class StartScreenController {

    /**
     * Launches the game view.
     */
    public void startGame(ActionEvent actionEvent) {
        Node node = (Node) actionEvent.getSource();
        Stage primaryStage = (Stage) node.getScene().getWindow();

        final Presenter presenter = new Presenter();
        Group root = new Group();

        Scene scene = new Scene(root, 640, 360);

        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.SPACE) {
                    presenter.handleKeyPress(keyEvent);
                    presenter.getHelicopterView().getHelicopterSound().play(); // In this method for performance reasons
                }
            }
        });
        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.SPACE) {
                    presenter.handleKeyRelease(keyEvent);
                    presenter.getHelicopterView().getHelicopterSound().stop(); // In this method for performance reasons
                }
            }
        });

        presenter.setRoot(root);
        primaryStage.setTitle("Helicopter Game");
        primaryStage.setScene(scene);
        primaryStage.show();
        presenter.setUpGame();
    }

    /**
     * Launches the high scores view.
     */
    public void highScores(ActionEvent actionEvent) {
        Node node = (Node) actionEvent.getSource();
        Stage primaryStage = (Stage) node.getScene().getWindow();

        final HighScoresPresenter hsController = new HighScoresPresenter();
        Group root = new Group();

        Scene scene = new Scene(root, 640, 360);

        hsController.setRoot(root);
        primaryStage.setTitle("Helicopter Game");
        primaryStage.setScene(scene);
        primaryStage.show();
        hsController.createScoreLabels();

    }
}
