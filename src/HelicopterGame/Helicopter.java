package HelicopterGame;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;

/**
 * Created by Grant Terrien
 *
 * Represents the model of the player character, the helicopter.
 */
public class Helicopter {
    private Point2D locationPoint;
    private int yVelocity;
    private BoundingBox boundingBox;



    public Helicopter(Point2D locationPoint) {
        this.locationPoint = locationPoint;
        this.yVelocity = 0;
        this.boundingBox = new BoundingBox(locationPoint.getX(),locationPoint.getY(),40,40);
    }

    /**
     *
     * @param point Point2D to set location
     */
    public void setLocationPoint(Point2D point) {
        this.locationPoint = point;

        // Does this work?
        this.boundingBox = new BoundingBox(locationPoint.getX(),locationPoint.getY(),40,40);
    }

    /**
     *
     * @return current Point2D location
     */
    public Point2D getLocationPoint() {
        return this.locationPoint;
    }

    /**
     *
     * @param vel Velocity to set
     */
    public void setYVelocity(int vel) {
        this.yVelocity = vel;
    }

    /**
     *
     * @return Current Y velocity
     */
    public int getYVelocity() {
        return this.yVelocity;
    }

    public BoundingBox getBoundingBox() {
        return this.boundingBox;
    }

}
