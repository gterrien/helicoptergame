package HelicopterGame;

import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ScoreListTest {


    @Test
    public void testSaveAndLoad() throws Exception {
        ScoreList scoreList = new ScoreList();
        scoreList.addNewScore(30);
        scoreList.addNewScore(60);
        scoreList.addNewScore(120);
        scoreList.save();

        ScoreList loadedScoreList = ScoreList.load();

        Assert.assertEquals(scoreList.getScores(),loadedScoreList.getScores());
    }

    @Test
    public void testAddNewScore() throws Exception {
        ScoreList scoreList = new ScoreList();
        scoreList.addNewScore(20);
        scoreList.addNewScore(50);

        ArrayList<Integer> testList = new ArrayList<Integer>();
        testList.add(new Integer(20));
        testList.add(new Integer(50));

        Assert.assertEquals(testList,scoreList.getScores());
    }

    @Test
    public void testToIntArray() throws Exception {
        ScoreList scoreList = new ScoreList();
        scoreList.addNewScore(5);
        scoreList.addNewScore(10);
        int[] array = new int[2];
        array[0] = 5;
        array[1] = 10;
        Assert.assertEquals(array[0],scoreList.toIntArray()[0]);
        Assert.assertEquals(array[1],scoreList.toIntArray()[1]);

    }
}