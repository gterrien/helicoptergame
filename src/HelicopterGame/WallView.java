package HelicopterGame;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by Grant Terrien
 *
 * View/sprite for Walls
 */
public class WallView extends Group {
    private Rectangle rectangle;

    public WallView(int height) {
        this.rectangle = new Rectangle(50.0, height);
        this.rectangle.setFill(Color.GREEN);

        this.getChildren().add(this.rectangle);
    }

    public Point2D getPosition() {
        Point2D position = new Point2D(this.getLayoutX(), this.getLayoutY());
        return position;
    }

    public void setPosition(double x, double y) {
        this.setLayoutX(x);
        this.setLayoutY(y);
    }
}
