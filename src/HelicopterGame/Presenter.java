package HelicopterGame;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;


/**
 * Created by Grant Terrien
 *
 * Controls the game state and updates the view according to the game state
 *
 * Named "Presenter" as according to the Model-View-Presenter design.
 */
public class Presenter {
    private Model model;
    private Label scoreLabel;
    private HelicopterView helicopterView;
    private ArrayList<WallView> wallViews;
    private Group root;
    private Timeline updater;
    private Timeline wallUpdater;
    private boolean spacePushed;


    final private int FRAMES_PER_SECOND = 60;

    public Presenter() {
    }


    public HelicopterView getHelicopterView() {
        return this.helicopterView;
    }

    public void setRoot(Group group) {
        this.root = group;
    }


    /**
     * Sets up two Timeline objects: one steps the model forward, one creates a new wall
     */
    public void setUpAnimationTimer() {
        updater = new Timeline(new KeyFrame(Duration.millis(1000/FRAMES_PER_SECOND), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                updateAnimation();
            }
        }));

        wallUpdater = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                updateWithNewWall();
            }
        }));

        updater.setCycleCount(Timeline.INDEFINITE);
        updater.play();

        wallUpdater.setCycleCount(Timeline.INDEFINITE);
        wallUpdater.play();
    }


    /**
     * Called by the animation timer once every *60* seconds to update the model and the view.
     */
    public void updateAnimation() {
        if (spacePushed) {
            model.raiseHelicopter();
            helicopterView.setRotate(-5.0);
        } else {
            helicopterView.setRotate(5.0);
        }

        model.step();
        helicopterView.setPosition(model.getHelicopter().getLocationPoint().getX(),
                model.getHelicopter().getLocationPoint().getY());
        updateWalls();
        scoreLabel.setText(Integer.toString(model.getCurrentScore()));

        if (model.helicopterHasCollided()) {
            endGame();
        }

    }


    /**
     * Spacebar pushed --> setSpacePushed(true)
     */
    public void handleKeyPress(KeyEvent event) {
        KeyCode code = event.getCode();
        if (code == KeyCode.SPACE) {
            spacePushed = true;
        }
    }

    /**
     * Spacebar released --> setSpacePushed(false)
     */
    public void handleKeyRelease(KeyEvent event) {
        KeyCode code = event.getCode();
        if (code == KeyCode.SPACE) {
            spacePushed = false;
        }
    }



    /**
     * Adds a new wall to the game model AND its corresponding view
     */
    public void updateWithNewWall() {
        Wall newestWall = model.createNewWall();
        WallView newestWallView = new WallView(newestWall.getHeight());
        newestWallView.setPosition(newestWall.getLocationPoint().getX(),
                newestWall.getLocationPoint().getY());
        wallViews.add(newestWallView);
        root.getChildren().add(newestWallView);
    }

    /**
     * Updates the position of the wall views based on the model
     */
    public void updateWalls() {
        for (int i = 0; i < model.getWallList().size(); i++) {
            wallViews.get(i).setPosition(model.getWallList().get(i).getLocationPoint().getX(),
                    model.getWallList().get(i).getLocationPoint().getY());
            if (model.getWallList().get(i).getLocationPoint().getX() < -100) {
                model.getWallList().remove(i);
                root.getChildren().remove(wallViews.get(i));
                wallViews.remove(i);
            }
        }
    }

    /**
     * Draws a label that indicates the current score
     */
    public void addScoreLabel() {
        scoreLabel = new Label();
        scoreLabel.setLayoutX(600);
        scoreLabel.setLayoutY(10);
        scoreLabel.setText("0");
        root.getChildren().add(scoreLabel);
    }

    /**
     * Starts the game with initial views
     */
    public void setUpGame() {
        model = new Model();
        helicopterView = new HelicopterView();
        helicopterView.setPosition(model.getHelicopter().getLocationPoint().getX(),
                model.getHelicopter().getLocationPoint().getY());
        root.getChildren().add(helicopterView);
        wallViews = new ArrayList<WallView>();
        addScoreLabel();
        setUpAnimationTimer();

    }

    /**
     * Goes to the start screen view
     */
    public void loadStartScreen(Event actionEvent) {
        Node node = (Node) actionEvent.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("StartScreen.fxml"));/* Exception */
        } catch (Exception e) {
            System.out.println("Error loading StartScreen.fxml");
        }
        Scene scene = new Scene(root, 640, 360);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Adds a button to the screen that leads back to the start menu
     */
    public void addExitButton() {
        Button backToHome = new Button();
        backToHome.setText("Back");
        backToHome.setLayoutX(283.0);
        backToHome.setLayoutY(250.0);
        backToHome.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                loadStartScreen(mouseEvent);
            }
        });



        backToHome.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                // So that releasing space after losing doesn't go back to the start screen
                if (spacePushed) {
                    spacePushed = false;
                } else {
                loadStartScreen(keyEvent);
                }
            }
        });

        root.getChildren().add(backToHome);

    }

    /**
     * Moves the score label to front and center, and makes it much larger
     */
    public void magnifyScoreLabel() {
        scoreLabel.setLayoutX(300);
        scoreLabel.setLayoutY(180);
        scoreLabel.setScaleX(5);
        scoreLabel.setScaleY(5);
        scoreLabel.toFront();
        BackgroundFill backgroundFill = new BackgroundFill(Color.WHITE,null,null);
        Background background = new Background(backgroundFill);
        scoreLabel.setBackground(background);
    }

    /**
     * Stops the game, saves the score, etc.
     */
    public void endGame() {
        helicopterView.getHelicopterCrash().play();
        model.getScoreList().addNewScore(model.getCurrentScore());
        model.getScoreList().save();
        updater.stop();
        wallUpdater.stop();
        if (helicopterView.getHelicopterSound().isPlaying()) {
            helicopterView.getHelicopterSound().stop();
        }
        magnifyScoreLabel();
        helicopterView.helicopterDies();
        addExitButton();
    }



}
