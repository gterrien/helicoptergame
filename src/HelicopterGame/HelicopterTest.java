package HelicopterGame;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;
import junit.framework.Assert;

public class HelicopterTest {

    @org.junit.Test
    public void testSetLocationPoint() throws Exception {
        Helicopter helicopter = new Helicopter(new Point2D(100,180));
        helicopter.setLocationPoint(new Point2D(100,100));
        Assert.assertEquals(new Point2D(100,100),helicopter.getLocationPoint());
        Assert.assertEquals(100.0,helicopter.getBoundingBox().getMinX());
    }

    @org.junit.Test
    public void testGetLocationPoint() throws Exception {
        Helicopter helicopter = new Helicopter(new Point2D(100,180));
        Assert.assertEquals(new Point2D(100,180),helicopter.getLocationPoint());

    }

    @org.junit.Test
    public void testSetYVelocity() throws Exception {
        Helicopter helicopter = new Helicopter(new Point2D(100,180));
        helicopter.setYVelocity(200);
        Assert.assertEquals(200,helicopter.getYVelocity());
    }

    @org.junit.Test
    public void testGetYVelocity() throws Exception {
        Helicopter helicopter = new Helicopter(new Point2D(100,180));
        Assert.assertEquals(0,helicopter.getYVelocity());
    }

    @org.junit.Test
    public void testGetBoundingBox() throws Exception {
        Helicopter helicopter = new Helicopter(new Point2D(100,180));
        Assert.assertEquals(new BoundingBox(100,180,40,40),helicopter.getBoundingBox());
    }
}