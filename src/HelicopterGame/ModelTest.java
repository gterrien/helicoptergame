package HelicopterGame;

import javafx.geometry.Point2D;
import junit.framework.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ModelTest {

    @Test
    public void testCreateNewWall() throws Exception {
        Model model = new Model();
        model.createNewWall();
        Wall wall = model.getWallList().get(model.getWallList().size()-1);

        Assert.assertTrue(wall.getHeight() >= 60 && wall.getHeight() <= 300
                && wall.getLocationPoint().getY() <= 360 && wall.getLocationPoint().getY() >= 0
                && wall.getLocationPoint().getX() == 640);
    }

    @Test
    public void testHelicopterHasCollided() throws Exception {
        Model model = new Model();
        model.createNewWall();
        for (Wall wall : model.getWallList()) {
            wall.setLocationPoint(new Point2D(100,100));
        }
        model.getHelicopter().setLocationPoint(new Point2D(90,100));

        Assert.assertTrue(model.helicopterHasCollided());

        for (Wall wall : model.getWallList()) {
            wall.setLocationPoint(new Point2D(500,100));
        }

        Assert.assertFalse(model.helicopterHasCollided());

    }

    @Test
    public void testRaiseHelicopter() throws Exception {
        Model model = new Model();
        model.raiseHelicopter();
        model.raiseHelicopter();

        Assert.assertEquals(-40,model.getHelicopter().getYVelocity());
    }

    @Test
    public void testActGravityOnHelicopter() throws Exception {
        Model model = new Model();
        model.raiseHelicopter();
        model.actGravityOnHelicopter();

        Assert.assertEquals(-39,model.getHelicopter().getYVelocity());
    }

    @Test
    public void testStep() throws Exception {
        Model model = new Model();
        model.step();

        Assert.assertEquals(224.0,model.getHelicopter().getLocationPoint().getY());

        Assert.assertEquals(1,model.getCurrentScore());
    }
}