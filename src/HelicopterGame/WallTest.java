package HelicopterGame;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;
import junit.framework.Assert;
import org.junit.Test;

public class WallTest {

    @Test
    public void testGetLocationPoint() throws Exception {
        Wall wall = new Wall(new Point2D(300,300),100);
        Assert.assertEquals(new Point2D(300,300), wall.getLocationPoint());
    }

    @Test
    public void testSetLocationPoint() throws Exception {
        Wall wall = new Wall(new Point2D(300,300),100);
        wall.setLocationPoint(new Point2D(30,30));
        Assert.assertEquals(new Point2D(30,30),wall.getLocationPoint());
        Assert.assertEquals(30.0,wall.getBoundingBox().getMinX());
    }

    @Test
    public void testGetHeight() throws Exception {
        Wall wall = new Wall(new Point2D(300,300),100);
        Assert.assertEquals(100,wall.getHeight());
    }

    @Test
    public void testSetHeight() throws Exception {
        Wall wall = new Wall(new Point2D(300,300),100);
        wall.setHeight(160);
        Assert.assertEquals(160,wall.getHeight());

    }

    @Test
    public void testGetBoundingBox() {
        Wall wall = new Wall(new Point2D(300,300),100);
        Assert.assertEquals(new BoundingBox(300,300,50,100),wall.getBoundingBox());
    }
}