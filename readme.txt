HelicopterGame
Grant Terrien

Core Classes:
    •Model
    •Helicopter
    •Wall
    •ScoreList

What works:
 The game works well. The graphics/animation and sound are a bit crude but I
 think they get the point across in terms of communicating to the player how 
 they are controlling the helicopter.

What doesn't:
 Getting FXML files to play nicely with their controllers was really
 difficult for me so I only used FXML for the start screen. I don't think this
 makes much difference in terms of the outcome, because the game is simple
 enough that it really isn't a big deal. If one wanted to switch it to using
 FXML, it would be reasonably easy to adapt it. I also don't think FXML is a
 very good solution for game programming because games (especially ones like
 this) do not have many statically-located elements.

 I also do not have automated tests for any of the non-model classes because all
 of the non-trivial methods in those classes just update the GUI.
